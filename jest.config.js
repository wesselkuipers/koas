module.exports = {
  collectCoverageFrom: ['packages/**/*.ts'],
  moduleNameMapper: {
    [/^(koas-.*)$/.source]: '<rootDir>/packages/$1/src',
  },
  snapshotSerializers: ['<rootDir>/jest-string'],
  testEnvironment: 'node',
  transform: {
    [/\.ts$/.source]: 'ts-jest',
  },
};
