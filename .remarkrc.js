const fs = require('fs');
const path = require('path');

const dictionary = require('dictionary-en-us');
const noShortcutReferenceLink = require('remark-lint-no-shortcut-reference-link');
const english = require('retext-english');
const repeatedWords = require('retext-repeated-words');
const retext = require('remark-retext');
const spell = require('retext-spell');
const syntaxURLs = require('retext-syntax-urls');
const usage = require('retext-usage');
const unified = require('unified');

const personal = fs.readFileSync(path.join(__dirname, 'personal.dic'));

exports.plugins = [
  noShortcutReferenceLink,
  [
    retext,
    unified().use({
      plugins: [english, syntaxURLs, [spell, { dictionary, personal }], repeatedWords, usage],
    }),
  ],
];
