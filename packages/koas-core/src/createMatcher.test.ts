import createMatcher from './createMatcher';

it.each([
  ['/', '/', {}],
  ['/{foo}', '/bar', { foo: 'bar' }],
  ['/a/{a}/b/{b}/c/{c}', '/a/x/b/y/c/z', { a: 'x', b: 'y', c: 'z' }],
  ['/', '/todeep', null],
  ['/{foo}', '/', null],
])('%s should transform %s into %o', (template: string, url: string, params) => {
  const matcher = createMatcher(template);
  const result = matcher(url);
  // Test twice, because some RegExp functions hold a global state.
  expect(result).toStrictEqual(params);
  expect(result).toStrictEqual(params);
});
