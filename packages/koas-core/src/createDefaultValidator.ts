import { OpenAPIV3 } from 'openapi-types';
import * as ZSchema from 'z-schema';

import SchemaValidationError from './SchemaValidationError';

ZSchema.registerFormat('binary', () => true);

export type Validator = (data: any, schema: OpenAPIV3.SchemaObject) => Promise<boolean>;

export default function createDefaultValidator(): Validator {
  const validator = new ZSchema({
    assumeAdditional: true,
    breakOnFirstError: false,
    reportPathAsArray: true,
  });
  return (data, schema) => {
    return new Promise((resolve, reject) => {
      validator.validate(data, schema, (errors, valid) => {
        if (valid) {
          resolve(true);
        } else {
          reject(new SchemaValidationError('JSON schema validation failed', errors));
        }
      });
    });
  };
}
