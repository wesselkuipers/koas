export default class SchemaValidationError extends Error {
  public errors: any[];

  public constructor(message: string, errors: any[]) {
    super(message);
    this.name = 'SchemaValidationError';
    this.errors = errors;
    Error.captureStackTrace(this, this.constructor);
  }
}
