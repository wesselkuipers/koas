# Koas-core

> [Koa][] + [Open API Specification][] = Koas

Koas aims to make it easy to write a RESTful API based on Koa and an Open API V3 Specification.

This is the core package. For more details, see the
[mono repository README](https://gitlab.com/remcohaszing/koas)

## Installation

```sh
npm install koa koas-core
```

## Usage

```js
const Koa = require('koa');
const koas = require('koas-core');

const spec = {
  openapi: '3.0.2',
  info: {
    title: 'My API',
    version: '1.0.0',
  },
  paths: {
    '/': {
      get: {
        responses: {},
      },
    },
  },
};

async function main() {
  const app = new Koa();
  app.use(
    await koas(spec, [
      // Koas plugins go here.
    ]),
  );
  app.listen(3333);
}

main().catch(error => {
  console.error(error);
  process.exit(1);
});
```

## Plugins

Plugins are functions that take parameters provided by Koas, and return a Koa middleware. Typically,
plugins are returned by a function, so options can be passed in.

Example plugin:

```js
module.exports = function myPlugin(options) {
  return koasPluginOptions => async (ctx, next) => {
    await next();
  };
};
```

TypeScript typings are provided. So it is also possible to write plugins using TypeScript. Pretty
much the only type annotation needed is `koas.Plugin` as the return option for the function.

```ts
import * as koas from 'koas-core';

function myPlugin(options: myPlugin.Options): koas.Plugin {
  return koasPluginOptions => async (ctx, next) => {
    await next();
  };
}

export = myPlugin;

namespace myPlugin {
  export interface Options {}
}
```
