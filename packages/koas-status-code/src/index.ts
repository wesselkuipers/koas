import * as Koas from 'koas-core';
import { OpenAPIV3 } from 'openapi-types';

const methods = ['delete', 'get', 'head', 'options', 'patch', 'post', 'put', 'trace'];

function makeStatusMap(spec: OpenAPIV3.Document): Map<OpenAPIV3.OperationObject, number> {
  const statusMap = new Map();
  Object.entries(spec.paths).forEach(([pathTemplate, pathItem]) => {
    methods.filter(Object.prototype.hasOwnProperty.bind(pathItem)).forEach(method => {
      const operationObject: OpenAPIV3.OperationObject = pathItem[method];
      Object.keys(operationObject.responses)
        .map(Number)
        .filter(Number.isInteger)
        .filter(status => status >= 200 && status < 300)
        .forEach(statusCode => {
          if (statusMap.has(operationObject)) {
            throw new Error(
              `Operation "#/paths/${pathTemplate}/${method}" has defined multiple success status codes`,
            );
          }
          statusMap.set(operationObject, statusCode);
        });
    });
  });
  return statusMap;
}

function koasStatusCode(): Koas.Plugin {
  return ({ spec }) => {
    const statusCodeMap = makeStatusMap(spec);

    return async (ctx, next) => {
      const { operationObject } = ctx.openApi;
      await next();
      if (statusCodeMap.has(operationObject)) {
        ctx.status = statusCodeMap.get(operationObject);
      }
    };
  };
}

export = koasStatusCode;

namespace koasStatusCode {}
