import { AxiosTestInstance, createInstance } from 'axios-test-instance';
import * as Koa from 'koa';
import * as koas from 'koas-core';

import * as koasStatusCode from '.';

const spec = {
  openapi: '3.0.2',
  info: {
    title: 'test',
    version: 'test',
  },
  paths: {
    '/create': {
      post: {
        responses: {
          201: { description: '' },
          404: { description: '' },
        },
      },
    },
    '/explicit': {
      get: {
        responses: {
          201: { description: '' },
        },
      },
    },
  },
};

let api: AxiosTestInstance;

beforeEach(async () => {
  const app = new Koa();
  app.use(
    await koas(spec, [
      () => async (ctx, next) => {
        if (ctx.url === '/explicit') {
          ctx.status = 200;
        }
        await next();
      },
      koasStatusCode(),
    ]),
  );
  api = await createInstance(app);
});

afterEach(async () => {
  await api.close();
});

it('should set a default success status code', async () => {
  const response = await api.post('/create');
  expect(response.status).toBe(201);
});

it('should ignore when multiple success status codes are defined', async () => {
  const response = await api.get('/conflict');
  expect(response.status).toBe(404);
});
