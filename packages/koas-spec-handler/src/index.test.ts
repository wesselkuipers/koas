import { AxiosTestInstance, createInstance } from 'axios-test-instance';
import * as Koa from 'koa';
import * as koas from 'koas-core';

import * as koasSpecHandler from '.';

const spec = {
  openapi: '3.0.2',
  info: {
    title: 'koas-spec-handler',
    version: 'test',
  },
  paths: {},
};

let api: AxiosTestInstance;

afterEach(async () => {
  await api.close();
});

it('should serve the OpenAPI spec on /api.json by default', async () => {
  const app = new Koa();
  app.use(await koas(spec, [koasSpecHandler()]));
  api = await createInstance(app);
  const response = await api.get('/api.json');
  expect(response.data).toStrictEqual(spec);
});

it('should be possible to define a custom url', async () => {
  const app = new Koa();
  app.use(await koas(spec, [koasSpecHandler('/custom-url')]));
  api = await createInstance(app);
  const response = await api.get('/custom-url');
  expect(response.data).toStrictEqual(spec);
});

it('should call the next middleware if there is no match', async () => {
  const app = new Koa();
  app.use(await koas(spec, [koasSpecHandler()]));
  api = await createInstance(app);
  const response = await api.get('/');
  expect(response.status).toBe(404);
});
