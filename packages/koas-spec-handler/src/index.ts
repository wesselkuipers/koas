import * as Koas from 'koas-core';

declare module 'koas-core' {
  interface OpenAPIContext {
    specURL?: string;
  }
}

function koasSpecHandler(url = '/api.json'): Koas.Plugin {
  return ({ rawSpec, runAlways }) =>
    runAlways(async (ctx, next) => {
      ctx.openApi.specURL = url;
      if (ctx.url === url && ctx.method === 'GET') {
        ctx.body = rawSpec;
        return undefined;
      }
      return next();
    });
}

export = koasSpecHandler;

namespace koasSpecHandler {}
