import * as Koas from 'koas-core';
import { OpenAPIV3 } from 'openapi-types';
import * as typeis from 'type-is';

interface Serializers {
  [mimetype: string]: (body: any) => string | Buffer;
}

const defaultSerializers: Serializers = {
  'application/json': body => JSON.stringify(body),
  'text/*': body => (body instanceof Buffer ? body : `${body}`),
};

function koasContentType(serializers?: Serializers): Koas.Plugin {
  const allSerializers = {
    ...defaultSerializers,
    ...serializers,
  };

  const supportedMimeTypes = Object.keys(allSerializers).sort((a, b) => (a > b ? -1 : 1));

  return () => async (ctx, next) => {
    await next();
    const { body, request } = ctx;
    const { operationObject } = ctx.openApi;
    const responseObject = (operationObject.responses[ctx.status] ||
      operationObject.responses.default) as OpenAPIV3.ResponseObject;
    if (responseObject && responseObject.content) {
      const contentType = request.accepts(Object.keys(responseObject.content));
      ctx.assert(contentType, 406);
      const type = supportedMimeTypes.find(m => !!typeis.is(contentType as string, m));
      // If type is null, this means a type was specified in the api specification, but no
      // serializer has been registered. Let’s assume in this case a developer has serialized the
      // body some other way.
      if (type != null) {
        const serializer = allSerializers[type];
        ctx.body = serializer(body);
      }
    }
  };
}

export = koasContentType;

namespace koasContentType {}
