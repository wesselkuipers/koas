import { Context, SchemaValidationError, Validator } from 'koas-core';
import { OpenAPIV3 } from 'openapi-types';

function createSchema(definitions: OpenAPIV3.ParameterObject[]): OpenAPIV3.SchemaObject {
  if (definitions.length === 0) {
    return null;
  }
  const r = definitions.filter(({ required }) => required).map(({ name }) => name);
  const fullSchema: OpenAPIV3.SchemaObject = {
    type: 'object',
    additionalProperties: true,
    properties: definitions.reduce((acc, { name, schema = {} }) => {
      acc[name] = schema;
      return acc;
    }, {}),
  };
  if (r.length) {
    fullSchema.required = r;
  }
  return fullSchema;
}

export default function createParameterHandler(
  operationObject: OpenAPIV3.OperationObject,
  parameters: OpenAPIV3.ParameterObject[],
  parsers: {},
  validate: Validator,
): (ctx: Context) => Promise<any> {
  const queryDefinitions = [];
  const pathDefinitions = [];
  [...(parameters || []), ...(operationObject.parameters || [])].forEach(
    (definition: OpenAPIV3.ParameterObject) => {
      if (definition.in === 'query') {
        queryDefinitions.push(definition);
      } else if (definition.in === 'path') {
        pathDefinitions.push(definition);
      }
    },
  );
  const querySchema = createSchema(queryDefinitions);
  const pathSchema = createSchema(pathDefinitions);

  function parse(type = 'string', value: any): any {
    if (Object.prototype.hasOwnProperty.call(parsers, type)) {
      return parsers[type](value);
    }
    return value;
  }

  return async ctx => {
    if (pathSchema) {
      ctx.params = Object.entries<string>(ctx.params).reduce((acc, [key, value]) => {
        if (!Object.prototype.hasOwnProperty.call(pathSchema.properties, key)) {
          acc[key] = value;
          return acc;
        }
        const { items, type = 'string' } = pathSchema.properties[key] as {
          items?: OpenAPIV3.SchemaObject;
          type?: string;
        };
        if (type === 'array') {
          acc[key] = value.split('/').map(val => parse(items && items.type, val));
          return acc;
        }
        acc[key] = parse(type, value);
        return acc;
      }, {});
      try {
        await validate(ctx.params, pathSchema);
      } catch (err) {
        if (!(err instanceof SchemaValidationError)) {
          throw err;
        }
        ctx.throw(400, err.message, err.errors);
      }
    }

    if (querySchema) {
      ctx.query = Object.entries<string | string[]>(ctx.query).reduce((acc, [key, value]) => {
        if (!Object.prototype.hasOwnProperty.call(querySchema.properties)) {
          acc[key] = value;
          return acc;
        }
        const { items, type = 'string' } = pathSchema.properties[key] as {
          items?: OpenAPIV3.SchemaObject;
          type?: string;
        };
        if (type === 'array') {
          const values = Array.isArray(value) ? value : [value];
          acc[key] = values.map(val => parse(items.type, val));
          return acc;
        }
        if (Array.isArray(value)) {
          acc[key] = value;
          return acc;
        }
        acc[key] = parse(type, value);
        return acc;
      }, {});
      try {
        await validate(ctx.query, querySchema);
      } catch (err) {
        if (!(err instanceof SchemaValidationError)) {
          throw err;
        }
        ctx.throw(400, err.message, err.errors);
      }
    }
  };
}
