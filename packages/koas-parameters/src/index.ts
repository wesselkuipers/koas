import * as Koas from 'koas-core';
import { OpenAPIV3 } from 'openapi-types';

import createParameterHandler from './createParameterHandler';

type Parser<T> = (value: string) => T | string;

interface ParameterParsers {
  boolean?: Parser<boolean>;
  integer?: Parser<number>;
  number?: Parser<number>;
  string?: Parser<string>;
}

const defaultParsers: ParameterParsers = {
  boolean(value) {
    switch (value) {
      case 'false':
        return false;
      case 'true':
        return true;
      default:
        return value;
    }
  },
  integer: value => parseInt(value, 10),
  number: parseFloat,
  string: String,
};

function koasParameters({ parsers = {} }: { parsers?: ParameterParsers } = {}): Koas.Plugin {
  const combinedParsers = {
    ...defaultParsers,
    ...parsers,
  };

  return ({ spec, validate }) => {
    const handlers = new Map<OpenAPIV3.OperationObject, (ctx: Koas.Context) => Promise<any>>();
    Object.values(spec.paths).forEach(
      ({ parameters, delete: del, get, head, options, patch, post, put, trace }) => {
        [del, get, head, options, patch, post, put, trace]
          .filter(Boolean)
          .forEach(operationObject => {
            handlers.set(
              operationObject,
              createParameterHandler(
                operationObject,
                parameters as OpenAPIV3.ParameterObject[],
                combinedParsers,
                validate,
              ),
            );
          });
      },
    );
    return async (ctx, next) => {
      const { operationObject } = ctx.openApi;
      if (handlers.has(operationObject)) {
        const handler = handlers.get(operationObject);
        await handler(ctx);
      }
      return next();
    };
  };
}

export = koasParameters;

namespace koasParameters {
  export type Parsers = ParameterParsers;
}
