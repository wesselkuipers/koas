import { AxiosTestInstance, createInstance } from 'axios-test-instance';
import * as Koa from 'koa';
import * as koas from 'koas-core';
import { OpenAPIV3 } from 'openapi-types';

import * as koasParameters from '.';

const spec: OpenAPIV3.Document = {
  openapi: '3.0.2',
  info: {
    title: 'Test server',
    version: 'test',
  },
  paths: {
    '/bool/{value}': {
      parameters: [
        {
          name: 'value',
          in: 'path',
          required: true,
          schema: { type: 'boolean' },
        },
      ],
      get: { responses: { 200: { description: '' } } },
    },
    '/int/{value}': {
      parameters: [
        {
          name: 'value',
          in: 'path',
          required: true,
          schema: { type: 'integer' },
        },
      ],
      get: { responses: { 200: { description: '' } } },
    },
    '/numeric/{value}': {
      parameters: [
        {
          name: 'value',
          in: 'path',
          required: true,
          schema: { type: 'number' },
        },
      ],
      get: { responses: { 200: { description: '' } } },
    },
    '/str/{value}': {
      parameters: [
        {
          name: 'value',
          in: 'path',
          required: true,
          schema: { type: 'string' },
        },
      ],
      get: { responses: { 200: { description: '' } } },
    },
  },
};

let api: AxiosTestInstance;
let params: Record<string, any>;

afterEach(() => {
  params = undefined;
});

describe('defaults', () => {
  beforeEach(async () => {
    const app = new Koa();
    app.use(
      await koas(spec, [
        koasParameters(),
        () => (ctx, next) => {
          ({ params } = ctx);
          return next();
        },
      ]),
    );
    api = await createInstance(app);
  });

  afterEach(async () => {
    await api.close();
  });

  it('should a boolean value true', async () => {
    await api.get('/bool/true');
    expect(params).toStrictEqual({ value: true });
  });

  it('should a boolean value false', async () => {
    await api.get('/bool/false');
    expect(params).toStrictEqual({ value: false });
  });

  it('should parse an integer', async () => {
    await api.get('/int/42');
    expect(params).toStrictEqual({ value: 42 });
  });

  it('should parse a number', async () => {
    await api.get('/numeric/13.37');
    expect(params).toStrictEqual({ value: 13.37 });
  });

  it('should leave a string as-is', async () => {
    await api.get('/str/1337');
    expect(params).toStrictEqual({ value: '1337' });
  });
});

describe('custom parsers', () => {
  beforeEach(async () => {
    const app = new Koa();
    app.use(
      await koas(spec, [
        koasParameters({ parsers: { string: value => `${value}-postfix` } }),
        () => (ctx, next) => {
          ({ params } = ctx);
          return next();
        },
      ]),
    );
    api = await createInstance(app);
  });

  afterEach(async () => {
    await api.close();
  });

  it('should leave a string as-is', async () => {
    await api.get('/str/value');
    expect(params).toStrictEqual({ value: 'value-postfix' });
  });
});
