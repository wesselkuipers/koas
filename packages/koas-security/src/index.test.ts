import { AxiosTestInstance, createInstance } from 'axios-test-instance';
import * as Koa from 'koa';
import * as koas from 'koas-core';
import { OpenAPIV3 } from 'openapi-types';

import * as koasSecurity from '.';
import { Clients, OAuth2Client, Users } from './types';

interface User {
  userId?: string;
  username?: string;
  password?: string;
  accessToken?: string;
}

const spec: OpenAPIV3.Document = {
  openapi: '3.0.2',
  info: {
    title: 'Test server',
    version: 'test',
  },
  components: {
    securitySchemes: {
      apiKeyCookieScheme: {
        type: 'apiKey',
        in: 'cookie',
        name: 'mycookie',
      },
      apiKeyHeaderScheme: {
        type: 'apiKey',
        in: 'header',
        name: 'my-header',
      },
      apiKeyQueryScheme: {
        type: 'apiKey',
        in: 'query',
        name: 'myQueryParameter',
      },
      httpBasicScheme: {
        type: 'http',
        scheme: 'basic',
      },
      httpBearerScheme: {
        type: 'http',
        scheme: 'bearer',
      },
      oauth2Scheme: {
        type: 'oauth2',
        flows: {},
      },
    },
  },
  paths: {
    '/api-key': {
      get: {
        responses: { 200: { description: '' } },
        security: [
          { apiKeyHeaderScheme: [], apiKeyQueryScheme: [] },
          { apiKeyCookieScheme: [] },
          { apiKeyHeaderScheme: [] },
          { apiKeyQueryScheme: [] },
        ],
      },
    },
    '/optional-api-key': {
      get: {
        responses: { 200: { description: '' } },
        security: [{ headerScheme: [] }, {}],
      },
    },
    '/http': {
      get: {
        responses: { 200: { description: '' } },
        security: [{ httpBasicScheme: [] }, { httpBearerScheme: [] }],
      },
    },
    '/oauth2': {
      get: {
        responses: { 200: { description: '' } },
        security: [{ oauth2Scheme: ['admin'] }, { oauth2Scheme: ['read', 'write'] }],
      },
    },
  },
};

let api: AxiosTestInstance;
let users: Users<User>;
let clients: Clients;
let clientScope: string | string[];

beforeEach(async () => {
  const app = new Koa();
  app.use(
    await koas(spec, [
      koasSecurity<User>({
        apiKeyCookieScheme: (apiKey: string) => (apiKey ? { userId: 'cookie user' } : null),
        apiKeyHeaderScheme: (apiKey: string) => (apiKey ? { userId: 'header user' } : null),
        apiKeyQueryScheme: (apiKey: string) => (apiKey ? { userId: 'query user' } : null),
        httpBasicScheme: (username, password) => (username ? { username, password } : null),
        httpBearerScheme: (accessToken: string) => (accessToken ? { accessToken } : null),
        oauth2Scheme: (accessToken: string): [User, OAuth2Client] =>
          accessToken ? [{ userId: 'OAuth2 user' }, { scope: clientScope }] : null,
      }),
      () => (ctx, next) => {
        ({ clients, users } = ctx);
        ctx.status = 200;
        return next();
      },
    ]),
  );
  api = await createInstance(app);
});

afterEach(async () => {
  clients = undefined;
  clientScope = undefined;
  users = undefined;
  await api.close();
});

it('should support a combination of authentication methods', async () => {
  const { status } = await api.get('/api-key?myQueryParameter=foo', {
    headers: { 'My-header': 'bar' },
  });
  expect(users).toStrictEqual({
    apiKeyHeaderScheme: { userId: 'header user' },
    apiKeyQueryScheme: { userId: 'query user' },
  });
  expect(status).toBe(200);
});

it('should not fail if an empty security requirement is allowed', async () => {
  const { status } = await api.get('/optional-api-key');
  expect(users).toStrictEqual({});
  expect(status).toBe(200);
});

it('should block a request is no security requirements are met', async () => {
  const { status } = await api.get('/api-key');
  expect(status).toBe(401);
});

describe('apiKey', () => {
  it('should support cookies', async () => {
    const { status } = await api.get('/api-key', { headers: { Cookie: 'mycookie=omnomnom' } });
    expect(users).toStrictEqual({ apiKeyCookieScheme: { userId: 'cookie user' } });
    expect(status).toBe(200);
  });

  it('should support headers', async () => {
    const { status } = await api.get('/api-key', { headers: { 'My-Header': 'bar' } });
    expect(users).toStrictEqual({ apiKeyHeaderScheme: { userId: 'header user' } });
    expect(status).toBe(200);
  });

  it('should support query parameters', async () => {
    const { status } = await api.get('/api-key?myQueryParameter=foo');
    expect(users).toStrictEqual({ apiKeyQueryScheme: { userId: 'query user' } });
    expect(status).toBe(200);
  });
});

describe('http', () => {
  it('should support basic auth', async () => {
    const { status } = await api.get('/http', { auth: { username: 'me', password: 'secret' } });
    expect(users).toStrictEqual({ httpBasicScheme: { username: 'me', password: 'secret' } });
    expect(status).toBe(200);
  });

  it('should support bearer tokens', async () => {
    const { status } = await api.get('/http', {
      headers: { Authorization: 'Bearer super.secret.token' },
    });
    expect(users).toStrictEqual({ httpBearerScheme: { accessToken: 'super.secret.token' } });
    expect(status).toBe(200);
  });
});

describe('oauth2', () => {
  it('should support client scope as string', async () => {
    clientScope = 'admin';
    const { status } = await api.get('/oauth2', {
      headers: { authorization: 'Bearer super.secret.token' },
    });
    expect(users).toStrictEqual({ oauth2Scheme: { userId: 'OAuth2 user' } });
    expect(clients).toStrictEqual({ oauth2Scheme: { scope: 'admin' } });
    expect(status).toBe(200);
  });

  it('should support multiple client scopes as string', async () => {
    clientScope = 'read write';
    const { status } = await api.get('/oauth2', {
      headers: { authorization: 'Bearer super.secret.token' },
    });
    expect(users).toStrictEqual({ oauth2Scheme: { userId: 'OAuth2 user' } });
    expect(clients).toStrictEqual({ oauth2Scheme: { scope: 'read write' } });
    expect(status).toBe(200);
  });

  it('should support multiple client scopes as an array', async () => {
    clientScope = ['read', 'write'];
    const { status } = await api.get('/oauth2', {
      headers: { authorization: 'Bearer super.secret.token' },
    });
    expect(users).toStrictEqual({ oauth2Scheme: { userId: 'OAuth2 user' } });
    expect(clients).toStrictEqual({ oauth2Scheme: { scope: ['read', 'write'] } });
    expect(status).toBe(200);
  });

  it('should reject if not all scopes are granted', async () => {
    clientScope = ['read'];
    const { status } = await api.get('/oauth2', {
      headers: { authorization: 'Bearer super.secret.token' },
    });
    expect(users).toBeUndefined();
    expect(clients).toBeUndefined();
    expect(status).toBe(401);
  });
});
