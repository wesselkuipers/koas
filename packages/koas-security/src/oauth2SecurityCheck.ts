import * as koas from 'koas-core';
import { OpenAPIV3 } from 'openapi-types';
import { Promisable } from 'type-fest';

import parseAuthorizationHeader from './parseAuthorizationHeader';
import { OAuth2Client, SecurityCheck } from './types';

export type GetOAuth2User<User> = (accessToken: string) => Promisable<[User, OAuth2Client]>;

export default function apiKeySecurityCheck<User>(
  scheme: OpenAPIV3.OAuth2SecurityScheme | OpenAPIV3.OpenIdSecurityScheme,
  clientGetter: GetOAuth2User<User>,
): SecurityCheck<User> {
  return async (ctx: koas.Context, scopes: string[]) => {
    const [type, accessToken] = parseAuthorizationHeader(ctx);
    if (type !== 'Bearer') {
      return null;
    }

    const pair = await clientGetter(accessToken);
    if (!pair || pair.length < 2) {
      return null;
    }
    const [user, client] = pair;
    const clientScopes = new Set(
      typeof client.scope === 'string' ? client.scope.split(/\s+/) : client.scope,
    );
    const isValid = scopes.every(scope => clientScopes.has(scope));
    return isValid ? ([user, client] as [User, OAuth2Client]) : null;
  };
}
