import * as koas from 'koas-core';
import { OpenAPIV3 } from 'openapi-types';
import { Promisable } from 'type-fest';

export interface OAuth2Client {
  scope: string | string[] | Set<string>;
}

export interface Clients {
  [securityScheme: string]: OAuth2Client;
}

export interface Users<User extends object> {
  [securityScheme: string]: User;
}

export type SecurityCheck<User> = (
  ctx: koas.Context,
  scopes?: string[],
) => Promisable<[User, OAuth2Client?]>;

declare module 'koas-core' {
  interface OpenAPIContext {
    securityRequirementObject?: OpenAPIV3.SecurityRequirementObject;
  }

  interface Context {
    clients?: Clients;
    users?: Users<object>;
  }
}
