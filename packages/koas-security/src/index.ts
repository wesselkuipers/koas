import * as Koas from 'koas-core';
import { OpenAPIV3 } from 'openapi-types';

import apiKeySecurityCheck, { GetApiKeyUser } from './apiKeySecurityCheck';
import httpSecurityCheck, { GetHttpUser } from './httpSecurityCheck';
import oauth2SecurityCheck, { GetOAuth2User } from './oauth2SecurityCheck';
import { Clients, SecurityCheck, Users } from './types';

type GetUser<User> = GetApiKeyUser<User> | GetHttpUser<User> | GetOAuth2User<User>;

function koasParameters<User extends {}>(userGetters: Record<string, GetUser<User>>): Koas.Plugin {
  return ({ spec }) => {
    const securityChecks: { [key: string]: SecurityCheck<User> } = {};
    Object.entries(spec.components.securitySchemes || []).forEach(
      ([key, scheme]: [string, OpenAPIV3.SecuritySchemeObject]) => {
        if (!userGetters[key]) {
          throw new Error(`No user getter was defined for security scheme: ${key}`);
        }
        switch (scheme.type) {
          case 'apiKey':
            securityChecks[key] = apiKeySecurityCheck(scheme, userGetters[key] as GetApiKeyUser<
              User
            >);
            break;
          case 'http':
            securityChecks[key] = httpSecurityCheck(scheme, userGetters[key] as GetHttpUser<User>);
            break;
          case 'oauth2':
          case 'openIdConnect':
            securityChecks[key] = oauth2SecurityCheck(scheme, userGetters[key] as GetOAuth2User<
              User
            >);
            break;
          default:
            throw new Error(`Unknown security scheme: ${JSON.stringify(scheme)}`);
        }
      },
    );

    return async (ctx, next) => {
      const { operationObject } = ctx.openApi;
      if (!operationObject || !operationObject.security) {
        return next();
      }
      for (const securityRequirementObject of operationObject.security) {
        const users: Users<User> = {};
        const clients: Clients = {};
        try {
          await Promise.all(
            Object.entries(securityRequirementObject).map(async ([key, requiredScopes]) => {
              const pair = await securityChecks[key](ctx, requiredScopes);
              if (!pair || !pair[0]) {
                throw new Error(`Unauthorized using security requirement: ${key}`);
              }
              [users[key]] = pair;
              if (pair[1]) {
                [, clients[key]] = pair;
              }
            }),
          );
        } catch (err) {
          continue;
        }
        ctx.users = users;
        ctx.clients = clients;
        ctx.openApi.securityRequirementObject = securityRequirementObject;
        return next();
      }
      return ctx.throw(401);
    };
  };
}

export = koasParameters;

namespace koasParameters {}
