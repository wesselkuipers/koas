import * as koas from 'koas-core';
import { OpenAPIV3 } from 'openapi-types';
import { Promisable } from 'type-fest';

import parseAuthorizationHeader from './parseAuthorizationHeader';
import { SecurityCheck } from './types';

type GetHttpBasicUser<User> = (username: string, password: string) => Promisable<User>;

type GetHttpBearerUser<User> = (accessToken: string) => Promisable<User>;

export type GetHttpUser<User> = GetHttpBasicUser<User> | GetHttpBearerUser<User>;

export default function httpSecurityCheck<User>(
  scheme: OpenAPIV3.HttpSecurityScheme,
  userGetter: GetHttpUser<User>,
): SecurityCheck<User> {
  return async (ctx: koas.Context) => {
    const [type, credentials] = parseAuthorizationHeader(ctx);
    let user: User;
    if (type.toLowerCase() !== scheme.scheme) {
      return null;
    }

    switch (type) {
      case 'Basic': {
        const basicAuthMatch = `${Buffer.from(credentials, 'base64')}`.match(/([^:]*):(.*)/);
        if (!basicAuthMatch) {
          return null;
        }
        user = await (userGetter as GetHttpBasicUser<User>)(basicAuthMatch[1], basicAuthMatch[2]);
        return [user] as [User];
      }
      case 'Bearer':
        user = await (userGetter as GetHttpBearerUser<User>)(credentials);
        return [user] as [User];
      default:
        // Unsupported authentication scheme.
        return null;
    }
  };
}
