import { Context } from 'koas-core';

const authorizationRegExp = /^(\S+) (.*)$/;

export default function parseAuthorizationHeader(ctx: Context): [string, string] {
  const match = authorizationRegExp.exec(ctx.header.authorization);
  if (!match) {
    return ['', null];
  }
  return match.slice(1) as [string, string];
}
