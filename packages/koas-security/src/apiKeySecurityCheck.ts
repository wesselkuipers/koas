import * as koas from 'koas-core';
import { OpenAPIV3 } from 'openapi-types';
import { Promisable } from 'type-fest';

import { SecurityCheck } from './types';

export type GetApiKeyUser<User> = (apiKey: string) => Promisable<User>;

export default function apiKeySecurityCheck<User>(
  scheme: OpenAPIV3.ApiKeySecurityScheme,
  userGetter: GetApiKeyUser<User>,
): SecurityCheck<User> {
  return async (ctx: koas.Context) => {
    let apiKey: string;

    switch (scheme.in) {
      case 'cookie':
        apiKey = ctx.cookies.get(scheme.name);
        break;
      case 'header':
        apiKey = ctx.headers[scheme.name.toLowerCase()];
        break;
      case 'query':
        apiKey = ctx.query[scheme.name];
        break;
      default:
        return null;
    }

    if (!apiKey) {
      return null;
    }

    const user = await userGetter(apiKey);
    return [user] as [User];
  };
}
