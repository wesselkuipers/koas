import * as Busboy from 'busboy';
import { Context, SchemaValidationError } from 'koas-core';
import { OpenAPIV3 } from 'openapi-types';
import { Readable } from 'stream';
import * as typeis from 'type-is';
import * as vfile from 'vfile';

function fromString(schema: OpenAPIV3.SchemaObject = { type: undefined }, content: string): any {
  switch (schema.type) {
    case 'integer':
    case 'number':
      return Number(content);
    case 'boolean':
      if (content === 'true') {
        return true;
      }
      if (content === 'false') {
        return false;
      }
      return content;
    case 'object':
      return JSON.parse(content);
    default:
      return content;
  }
}

async function formdata(
  body: Readable,
  mediaTypeObject: OpenAPIV3.MediaTypeObject = {},
  ctx: Context,
): Promise<{ [field: string]: any }> {
  const busboy = new Busboy(ctx.req);
  const { schema } = mediaTypeObject;
  const { encoding = {} } = mediaTypeObject;
  const { properties = {} } = (schema as OpenAPIV3.NonArraySchemaObject) || {};
  const response = {};
  const buffers = {};

  await new Promise((resolve, reject) => {
    function onError(error: Error): void {
      busboy.removeAllListeners();
      reject(error);
    }

    busboy
      .on('file', (fieldname, stream, filename, enc, mime) => {
        const buffer = [];
        stream.on('data', data => {
          buffer.push(data);
        });
        stream.on('end', () => {
          const vfileArgs: vfile.VFileOptions = {
            contents: Buffer.concat(buffer),
            mime,
          };
          if (filename != null) {
            vfileArgs.path = filename;
          }
          const data = vfile(vfileArgs);
          const propertySchema = properties[fieldname] as OpenAPIV3.SchemaObject;
          if (propertySchema && propertySchema.type === 'array') {
            if (!Object.prototype.hasOwnProperty.call(response, fieldname)) {
              response[fieldname] = [];
            }
            if (!Object.prototype.hasOwnProperty.call(buffers, fieldname)) {
              buffers[fieldname] = [];
            }
            response[fieldname].push('');
            buffers[fieldname].push(data);
          } else {
            response[fieldname] = '';
            buffers[fieldname] = data;
          }
        });
      })
      .on('field', (fieldname, content) => {
        const propertySchema = properties[fieldname] as OpenAPIV3.SchemaObject;
        if (propertySchema && propertySchema.type === 'array') {
          if (!Object.prototype.hasOwnProperty.call(response, fieldname)) {
            response[fieldname] = [];
          }
          response[fieldname].push(
            fromString(
              (propertySchema as OpenAPIV3.ArraySchemaObject).items as OpenAPIV3.SchemaObject,
              content,
            ),
          );
        } else {
          response[fieldname] = fromString(propertySchema, content);
        }
      })
      .on('finish', () => {
        busboy.removeAllListeners();
        resolve(response);
      })
      .on('error', onError)
      .on('partsLimit', onError)
      .on('filesLimit', onError)
      .on('fieldsLimit', onError);
    ctx.req.pipe(busboy);
  });

  await ctx.openApi.validate(response, schema as OpenAPIV3.SchemaObject);
  const bufferErrors = Object.entries(buffers).reduce((acc, [key, values]) => {
    const vals = Array.isArray(values) ? values : [values];
    vals.forEach((value, index) => {
      if (!Object.prototype.hasOwnProperty.call(encoding, key)) {
        return;
      }
      if (!Object.prototype.hasOwnProperty.call(encoding[key], 'contentType')) {
        return;
      }
      if (!typeis.is(value.mime, encoding[key].contentType.split(','))) {
        acc.push({
          code: 'INVALID_CONTENT_TYPE',
          params: [key, index],
        });
      }
    });
    return acc;
  }, []);
  if (bufferErrors.length) {
    throw new SchemaValidationError('Invalid content types found', bufferErrors);
  }
  return Object.assign(response, buffers);
}

formdata.skipValidation = true;
export default formdata;
