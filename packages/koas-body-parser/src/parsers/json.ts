import { Context } from 'koas-core';
import { OpenAPIV3 } from 'openapi-types';

import plain from './plain';

export default async (
  body,
  mediaTypeObject: OpenAPIV3.MediaTypeObject,
  ctx: Context,
): Promise<any> => JSON.parse(await plain(body, mediaTypeObject, ctx));
