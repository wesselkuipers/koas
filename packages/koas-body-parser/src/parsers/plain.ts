import { Context } from 'koas-core';
import { OpenAPIV3 } from 'openapi-types';
import * as raw from 'raw-body';
import { Readable } from 'stream';

export default (
  body: Readable,
  mediaTypeObject: OpenAPIV3.MediaTypeObject,
  ctx: Context,
): Promise<string> =>
  raw(body, {
    encoding: ctx.request.charset || 'utf8',
    length: ctx.request.length,
  });
