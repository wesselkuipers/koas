import { Context } from 'koas-core';
import { OpenAPIV3 } from 'openapi-types';
import { Readable } from 'stream';

import formdata from './formdata';
import json from './json';
import plain from './plain';

export type Parser = (
  body: Readable,
  mediaTypeObject: OpenAPIV3.MediaTypeObject,
  ctx: Context,
) => Promise<any>;

export interface Parsers {
  [mime: string]: Parser;
}

export default {
  'application/json': json,
  'multipart/form-data': formdata,
  'text/plain': plain,
};
