import * as inflate from 'inflation';
import * as Koas from 'koas-core';
import { OpenAPIV3 } from 'openapi-types';
import * as typeis from 'type-is';

import defaultParsers, { Parsers } from './parsers';

declare module 'koa' {
  interface Request {
    body: any;
    rawBody: string;
  }
}

function koasBodyParser(parsers: Parsers = {}): Koas.Plugin {
  const allParsers = {
    ...defaultParsers,
    ...parsers,
  };

  const supportedMimeTypes = Object.keys(allParsers).sort((a, b) => (a > b ? -1 : 1));

  return ({ validate }) => async (ctx, next) => {
    const { operationObject } = ctx.openApi;
    if (!operationObject.requestBody) {
      return next();
    }
    const { content, required } = operationObject.requestBody as OpenAPIV3.RequestBodyObject;
    const requestContentType = ctx.request.type || 'application/octet-stream';
    const is = typeis.is.bind(null, requestContentType);
    ctx.assert(is(Object.keys(content)), 415);
    const type = supportedMimeTypes.find(is);
    const parser = allParsers[type];
    ctx.assert(parser, 415);
    const mediaTypeObject = content[type];
    let body;
    try {
      body = await parser(inflate(ctx.req), mediaTypeObject, ctx);
      ctx.assert(!required || body !== undefined, 400, 'Missing request body');
      if (!parser.skipValidation && mediaTypeObject && mediaTypeObject.schema) {
        await validate(body, mediaTypeObject.schema as OpenAPIV3.SchemaObject);
      }
    } catch (err) {
      if (!(err instanceof Koas.SchemaValidationError)) {
        throw err;
      }
      ctx.body = {
        message: err.message,
        errors: err.errors,
      };
      ctx.status = 400;
      return undefined;
    }
    ctx.request.body = body;
    return next();
  };
}

export = koasBodyParser;

namespace koasBodyParser {}
