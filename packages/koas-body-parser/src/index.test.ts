import { AxiosTestInstance, createInstance } from 'axios-test-instance';
import * as Koa from 'koa';
import * as koas from 'koas-core';

import * as koasBodyParser from '.';

const spec = {
  openapi: '3.0.2',
  info: {
    title: 'test',
    version: 'test',
  },
  paths: {
    '/': {
      post: {
        requestBody: {
          content: {
            'application/json': {},
            'text/plain': {},
          },
        },
        responses: {
          default: {
            description: '',
            content: {},
          },
        },
      },
    },
  },
};

let api: AxiosTestInstance;
let body: any;

beforeEach(async () => {
  const app = new Koa();
  app.use(
    await koas(spec, [
      koasBodyParser(),
      () => async ctx => {
        ({ body } = ctx.request);
      },
    ]),
  );
  api = await createInstance(app);
});

afterEach(async () => {
  body = undefined;
  await api.close();
});

it('should transform application/json into a JavaScript object', async () => {
  await api.post('/', { text: 'This is json' });
  expect(body).toStrictEqual({ text: 'This is json' });
});

it('should transform text/plain into a regular string', async () => {
  await api.post('/', '{"text":"This is plain text"}', {
    headers: { 'content-type': 'text/plain' },
  });
  expect(body).toStrictEqual('{"text":"This is plain text"}');
});
