import { AxiosTestInstance, createInstance } from 'axios-test-instance';
import { readFileSync } from 'fs';
import * as Koa from 'koa';
import * as koas from 'koas-core';
import * as koasSpecHandler from 'koas-spec-handler';
import { join } from 'path';
import { absolutePath } from 'swagger-ui-dist';

import * as koasSwaggerUI from '.';

const assets = [
  'favicon-16x16.png',
  'favicon-32x32.png',
  'swagger-ui-bundle.js',
  'swagger-ui-bundle.js.map',
  'swagger-ui.css',
  'swagger-ui.css.map',
  'swagger-ui-standalone-preset.js',
  'swagger-ui-standalone-preset.js.map',
].reduce((acc, filename) => {
  acc[filename] = readFileSync(join(absolutePath(), filename));
  return acc;
}, {});

const spec = {
  openapi: '3.0.2',
  info: {
    title: 'Test server',
    version: 'test',
  },
  paths: {},
};

let api: AxiosTestInstance;

afterEach(async () => {
  await api.close();
});

describe.each([
  ['/', '/'],
  ['/foo', '/'],
  ['/foo/', '/foo/'],
  ['/foo/bar', '/foo/'],
  ['/foo/bar/', '/foo/bar/'],
])('given url %s', (url, prefix) => {
  it.each([
    ['favicon-16x16.png', 'image/png'],
    ['favicon-32x32.png', 'image/png'],
    ['swagger-ui-bundle.js', 'application/javascript; charset=utf-8'],
    ['swagger-ui-bundle.js.map', 'application/json; charset=utf-8'],
    ['swagger-ui.css', 'text/css; charset=utf-8'],
    ['swagger-ui.css.map', 'application/json; charset=utf-8'],
  ])(`should serve ${prefix}%s using mime type %s`, async (p, mime) => {
    const app = new Koa();
    app.use(await koas(spec, [koasSpecHandler(), koasSwaggerUI({ url })]));
    api = await createInstance(app);
    const response = await api.get(`${prefix}${p}`, { responseType: 'arraybuffer' });
    expect(response.headers['content-type']).toBe(mime);
    expect(response.data.equals(assets[p])).toBe(true);
  });
});

describe('defaults', () => {
  it('should render index.html', async () => {
    const app = new Koa();
    app.use(await koas(spec, [koasSpecHandler(), koasSwaggerUI()]));
    api = await createInstance(app);
    const response = await api.get('/');
    expect(response.headers['content-type']).toBe('text/html; charset=utf-8');
    expect(response.data).toMatchSnapshot();
  });
});

describe('trailing slash', () => {
  it('should render index.html', async () => {
    const app = new Koa();
    app.use(await koas(spec, [koasSpecHandler(), koasSwaggerUI({ url: '/prefix/' })]));
    api = await createInstance(app);
    const response = await api.get('/prefix/');
    expect(response.headers['content-type']).toBe('text/html; charset=utf-8');
    expect(response.data).toMatchSnapshot();
  });
});

describe('no trailing slash', () => {
  it('should render index.html', async () => {
    const app = new Koa();
    app.use(await koas(spec, [koasSpecHandler(), koasSwaggerUI({ url: '/prefix' })]));
    api = await createInstance(app);
    const response = await api.get('/prefix');
    expect(response.headers['content-type']).toBe('text/html; charset=utf-8');
    expect(response.data).toMatchSnapshot();
  });
});
