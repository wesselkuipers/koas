import 'koas-spec-handler';

import { readFile, readFileSync } from 'fs';
import * as Koas from 'koas-core';
import { template } from 'lodash';
import * as path from 'path';
import { absolutePath } from 'swagger-ui-dist';

import pluginMap from './plugins';
import presetMap from './presets';
import relativeRequestPath from './relativeRequestPath';

const renderIndex = template(readFileSync(path.resolve(__dirname, '..', 'index.html'), 'utf8'), {
  // This interpolation syntax allows to use both mustache syntax, which is nice for HTML content,
  // and ES6 template strings, which is nice for the inline JavaScript.
  interpolate: /(?:`\$|\{)\{([^\\}]*(?:\\.[^\\}]*)*)\}[`}]/,
});

const paths = [
  'favicon-16x16.png',
  'favicon-32x32.png',
  'oauth2-redirect.html',
  'swagger-ui-bundle.js',
  'swagger-ui-bundle.js.map',
  'swagger-ui.css',
  'swagger-ui.css.map',
];

function koasSwaggerUI({
  plugins = [pluginMap.DownloadUrl],
  presets = [presetMap.apis],
  url = '/',
}: {
  plugins?: string[];
  presets?: string[];
  url?: string;
} = {}): Koas.Plugin {
  const pathMap = new Map(
    paths.map<[string, { file: string; type: string }]>(p => [
      relativeRequestPath(url, p),
      { file: path.join(absolutePath(), p), type: path.parse(p).ext },
    ]),
  );

  return ({ runAlways, spec }) =>
    runAlways(async (ctx, next) => {
      const { specURL } = ctx.openApi;
      if (ctx.path === url) {
        ctx.body = renderIndex({
          plugins,
          presets,
          spec,
          specURL,
        });
        ctx.type = 'html';
        return undefined;
      }
      if (pathMap.has(ctx.path)) {
        const { file, type } = pathMap.get(ctx.path);
        ctx.body = await new Promise((resolve, reject) => {
          readFile(file, (err, content) => {
            if (err) {
              reject(err);
            } else {
              resolve(content);
            }
          });
        });
        ctx.type = type;
        return undefined;
      }
      return next();
    });
}

export = koasSwaggerUI;

namespace koasSwaggerUI {
  export const plugins = pluginMap;
  export const presets = presetMap;
}
