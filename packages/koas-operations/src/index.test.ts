import { AxiosTestInstance, createInstance } from 'axios-test-instance';
import * as Koa from 'koa';
import * as koas from 'koas-core';

import * as koasOperations from '.';

const spec = {
  openapi: '3.0.2',
  info: {
    title: 'Test server',
    version: 'test',
  },
  paths: {
    '/': {
      get: { operationId: 'test', responses: { 200: { description: '' } } },
      post: { responses: { 200: { description: '' } } },
      put: { operationId: 'invalid', responses: { 200: { description: '' } } },
    },
  },
};

let api: AxiosTestInstance;

it('should not crash when no options are passed', async () => {
  expect(koasOperations).not.toThrow();
});

describe('defaults', () => {
  let operations: { [operationId: string]: any };

  beforeEach(async () => {
    const app = new Koa();
    app.on('error', () => {});
    operations = {
      invalid: {},
      test: jest.fn(() => {}),
    };
    app.use(await koas(spec, [koasOperations({ operations })]));
    api = await createInstance(app);
  });

  afterEach(async () => {
    await api.close();
  });

  it('should call the matching operation', async () => {
    await api.get('/');
    expect(operations.test).toHaveBeenCalledTimes(1);
  });

  it('should handle undefined operation ids as not implemented', async () => {
    const response = await api.post('/');
    expect(response.status).toBe(501);
  });

  it('should handle non-function implementations as not implemented', async () => {
    const response = await api.put('/');
    expect(response.status).toBe(501);
  });
});

describe('custom notImplemented', () => {
  let fallback;

  beforeEach(async () => {
    fallback = jest.fn();
    const app = new Koa();
    app.use(await koas(spec, [koasOperations({ fallback })]));
    api = await createInstance(app);
  });

  afterEach(async () => {
    await api.close();
  });

  it('should call the fallback if an undefined operation is not implemented', async () => {
    await api.post('/');
    expect(fallback).toHaveBeenCalledTimes(1);
  });

  it('should call the fallback if an operation has a non-function implementation', async () => {
    await api.put('/');
    expect(fallback).toHaveBeenCalledTimes(1);
  });
});
