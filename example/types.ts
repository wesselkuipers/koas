import * as koas from 'koas-core';
import { Model, ModelCtor } from 'sequelize';

import { UserStatic } from './models/User';

export interface Argv {
  database: string;
  port: number;
  secret: string;
}

export interface JWTPayload {
  sub: string;
  exp: number;
}

export interface Models {
  Pet?: ModelCtor<Model>;
  User?: UserStatic;
}

export interface Context extends koas.Context {
  secret: string;
  models: Models;
}
