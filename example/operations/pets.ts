import { Context } from '../types';

export async function createPet(ctx: Context): Promise<any> {
  const { Pet } = ctx.models;
  ctx.body = await Pet.create(ctx.request.body);
}

export async function getPets(ctx: Context): Promise<any> {
  const { Pet } = ctx.models;
  ctx.body = await Pet.findAll({ raw: true });
}

export async function getPetById(ctx: Context): Promise<any> {
  const { Pet } = ctx.models;
  const { petId } = ctx.params;
  ctx.body = await Pet.findByPk(petId, { raw: true });
}

export async function patchPet(ctx: Context): Promise<any> {
  const { Pet } = ctx.models;
  const { petId } = ctx.params;
  const pet = await Pet.findByPk(petId);
  pet.set(ctx.request.body);
  await pet.save();
  ctx.body = pet.dataValues;
}

export async function updatePet(ctx: Context): Promise<any> {
  const { Pet } = ctx.models;
  const { petId } = ctx.params;
  const pet = await Pet.findByPk(petId);
  pet.set(ctx.request.body);
  await pet.save();
  ctx.body = pet.dataValues;
}

export async function deletePet(ctx: Context): Promise<any> {
  const { Pet } = ctx.models;
  const { petId } = ctx.params;
  const pet = await Pet.findByPk(petId);
  await pet.destroy();
}
