import { AxiosTestInstance, createInstance } from 'axios-test-instance';

import { createServer } from '../server';

let api: AxiosTestInstance;

beforeEach(async () => {
  api = await createInstance(await createServer({ database: 'sqlite://:memory:', secret: 'test' }));
  const { data } = await api.post('/register', { username: 'user', password: 'password' });
  api.defaults.headers.authorization = `Bearer ${data.accessToken}`;
});

afterEach(async () => {
  await api.close();
});

it('should be possible to create pets', async () => {
  const { data, status } = await api.post('/pets', { species: 'cat', name: 'Mittens' });
  expect(data).toStrictEqual({ id: expect.any(Number), species: 'cat', name: 'Mittens' });
  expect(status).toBe(201);
});

it('should be possible to retrieve pets', async () => {
  const { data: created } = await api.post('/pets', { species: 'cat', name: 'Mittens' });
  const { data: retrieved, status } = await api.get(`/pets/${created.id}`);
  expect(retrieved).toStrictEqual(created);
  expect(status).toBe(200);
});

it('should be possible to create and list', async () => {
  const { data: cat } = await api.post('/pets', { species: 'cat', name: 'Mittens' });
  const { data: dog } = await api.post('/pets', { species: 'dog', name: 'Brian' });
  const { data, status } = await api.get('/pets');
  expect(data).toStrictEqual([cat, dog]);
  expect(status).toBe(200);
});
