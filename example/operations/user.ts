import { randomBytes } from 'crypto';
import { sign } from 'jsonwebtoken';

import { Context } from '../types';

export async function registerUser(ctx: Context): Promise<any> {
  const { User } = ctx.models;
  const { username, password } = ctx.request.body;
  const apiKey = randomBytes(12).toString('base64');
  await User.create({ username, password, apiKey });
  const accessToken = sign(
    {
      exp: Math.floor(Date.now() / 1000) + 3600,
      sub: username,
    },
    ctx.secret,
  );
  ctx.body = {
    username,
    apiKey,
    accessToken,
  };
}
