import argv from './argv';
import { start } from './server';

if (module === require.main) {
  start(argv()).catch(error => {
    // eslint-disable-next-line no-console
    console.error(error);
    process.exit(error.code || 1);
  });
}
