import * as yargs from 'yargs';

import { Argv } from './types';

export default (): any =>
  (yargs as yargs.Argv<Argv>)
    .usage('Usage:\n  npm start')
    .option('database', {
      desc: 'The database URL to use.',
      default: 'sqlite://:memory:',
    })
    .option('port', {
      desc: 'The port on which to host the server.',
      type: 'number',
      default: 3333,
    })
    .option('secret', {
      desc: 'The app secret.',
      default: 'super-secret',
    }).argv;
