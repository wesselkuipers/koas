import { DataTypes, Model, Sequelize } from 'sequelize';

interface Pet {
  readonly id: number;
  name?: string;
  species?: string;
}

export type PetStatic = typeof Model & {
  new (values?: Partial<Pet>): Pet;
};

export default (sequelize: Sequelize): PetStatic => {
  return sequelize.define('Pet', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    name: DataTypes.STRING,
    species: DataTypes.STRING,
  }) as PetStatic;
};
