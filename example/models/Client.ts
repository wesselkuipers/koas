import { DataTypes, Model, Sequelize } from 'sequelize';

export interface Client {
  id: string;
  secret: string;
}

export type ClientStatic = typeof Model & {
  new (values?: Partial<Client>): Client;
};

export default (sequelize: Sequelize): ClientStatic => {
  return sequelize.define('Client', {
    id: {
      primaryKey: true,
      type: DataTypes.STRING,
    },
    secret: {
      type: DataTypes.STRING,
    },
    user: {
      type: DataTypes.STRING,
    },
  }) as ClientStatic;
};
