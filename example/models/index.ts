import { Sequelize } from 'sequelize';

import { Models } from '../types';
import registerClient from './Client';
import registerPet from './Pet';
import registerUser from './User';

export default async (connectionURL: string): Promise<Models> => {
  const sequelize = new Sequelize(connectionURL, { define: { timestamps: false }, logging: false });
  registerPet(sequelize);
  const Client = registerClient(sequelize);
  const User = registerUser(sequelize);
  Client.hasOne(User);
  await sequelize.sync();
  return sequelize.models;
};
