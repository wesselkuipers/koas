import { DataTypes, Model, Sequelize } from 'sequelize';

export interface User {
  username: string;
  password: string;
  apiKey: string;
}

export type UserStatic = typeof Model & {
  new (values?: Partial<User>): User;
};

export default (sequelize: Sequelize): UserStatic => {
  return sequelize.define('User', {
    username: {
      primaryKey: true,
      type: DataTypes.STRING,
    },
    password: {
      type: DataTypes.STRING,
    },
    apiKey: {
      type: DataTypes.STRING,
    },
  }) as UserStatic;
};
