import * as fs from 'fs';
import { OpenAPIV3 } from 'openapi-types';
import * as parseAuthor from 'parse-author';
import * as path from 'path';

import { author, name, version } from '../package.json';

const document: OpenAPIV3.Document = {
  openapi: '3.0.2',
  info: {
    title: name,
    version,
    contact: parseAuthor(author),
    description: fs.readFileSync(path.resolve(__dirname, '../README.md'), 'utf8'),
  },
  components: {
    requestBodies: {
      Pet: {
        required: true,
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Pet',
            },
          },
          'multipart/form-data': {
            schema: {
              type: 'object',
              properties: {
                data: {
                  $ref: '#/components/schemas/Pet',
                },
                photo: {
                  type: 'string',
                  format: 'binary',
                },
              },
            },
          },
          'text/yaml': {
            schema: {
              $ref: '#/components/schemas/Pet',
            },
          },
        },
      },
      User: {
        required: true,
        content: {
          'application/json': {
            schema: {
              type: 'object',
              required: ['username', 'password'],
              properties: {
                username: {
                  type: 'string',
                },
                password: {
                  type: 'string',
                  // format: 'password',
                },
              },
            },
          },
        },
      },
    },
    responses: {
      GeneralError: {
        description: 'A general error message.',
      },
      Pet: {
        description: 'A successful pet response.',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Pet',
            },
          },
          'text/yaml': {
            schema: {
              $ref: '#/components/schemas/Pet',
            },
          },
        },
      },
      User: {
        description: 'A successful user response.',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                username: {
                  type: 'string',
                },
                apiKey: {
                  type: 'string',
                },
                accessToken: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
    schemas: {
      Pet: {
        type: 'object',
        example: {
          name: 'Mittens',
          species: 'cat',
        },
        required: ['name', 'species'],
        properties: {
          id: {
            type: 'number',
            readOnly: true,
            description: 'The generated unique identifier.',
          },
          name: {
            type: 'string',
            description: 'The name of the pet.',
          },
          species: {
            type: 'string',
            description: 'The species of the pet.',
          },
        },
      },
    },
    securitySchemes: {
      cookieAuth: {
        type: 'apiKey',
        in: 'cookie',
        name: 'apiKey',
      },
      headerAuth: {
        type: 'apiKey',
        in: 'header',
        name: 'apiKey',
      },
      queryAuth: {
        type: 'apiKey',
        in: 'query',
        name: 'apiKey',
      },
      basicAuth: {
        type: 'http',
        scheme: 'basic',
      },
      jwt: {
        type: 'http',
        scheme: 'bearer',
        bearerFormat: 'jwt',
      },
      oauth2: {
        type: 'oauth2',
        flows: {
          clientCredentials: {
            tokenUrl: '/token',
            scopes: {
              'pets:list': 'list all pets',
              'pets:write': 'edit pets in the database',
            },
          },
        },
      },
    },
  },
  paths: {
    '/pets': {
      post: {
        operationId: 'createPet',
        tags: ['pet'],
        requestBody: {
          $ref: '#/components/requestBodies/Pet',
        },
        responses: {
          201: {
            $ref: '#/components/responses/Pet',
          },
          default: {
            $ref: '#/components/responses/GeneralError',
          },
        },
        security: [
          { cookieAuth: [] },
          { headerAuth: [] },
          { queryAuth: [] },
          { basicAuth: [] },
          { jwt: [] },
        ],
      },
      get: {
        operationId: 'getPets',
        tags: ['pet'],
        responses: {
          200: {
            description: 'A list of pets.',
            content: {
              'application/json': {
                schema: {
                  type: 'array',
                  items: {
                    $ref: '#/components/schemas/Pet',
                  },
                },
              },
              'text/yaml': {
                schema: {
                  type: 'array',
                  items: {
                    $ref: '#/components/schemas/Pet',
                  },
                },
              },
            },
          },
          default: {
            $ref: '#/components/responses/GeneralError',
          },
        },
      },
    },
    '/pets/{petId}': {
      parameters: [
        {
          name: 'petId',
          in: 'path',
          required: true,
          description: 'The pet identifier.',
          schema: {
            type: 'number',
          },
        },
      ],
      get: {
        operationId: 'getPetById',
        tags: ['pet'],
        responses: {
          '200': {
            $ref: '#/components/responses/Pet',
          },
          default: {
            $ref: '#/components/responses/GeneralError',
          },
        },
      },
      patch: {
        operationId: 'patchPet',
        tags: ['pet'],
        requestBody: {
          $ref: '#/components/requestBodies/Pet',
        },
        responses: {
          200: {
            $ref: '#/components/responses/Pet',
          },
          default: {
            $ref: '#/components/responses/GeneralError',
          },
        },
        security: [
          { cookieAuth: [] },
          { headerAuth: [] },
          { queryAuth: [] },
          { basicAuth: [] },
          { jwt: [] },
        ],
      },
      put: {
        operationId: 'updatePet',
        tags: ['pet'],
        requestBody: {
          $ref: '#/components/requestBodies/Pet',
        },
        responses: {
          200: {
            $ref: '#/components/responses/Pet',
          },
          default: {
            $ref: '#/components/responses/GeneralError',
          },
        },
        security: [
          { cookieAuth: [] },
          { headerAuth: [] },
          { queryAuth: [] },
          { basicAuth: [] },
          { jwt: [] },
        ],
      },
      delete: {
        operationId: 'deletePet',
        tags: ['pet'],
        responses: {
          204: {
            description: 'If the pet has been deleted succesfully.',
          },
          default: {
            $ref: '#/components/responses/GeneralError',
          },
        },
        security: [
          { cookieAuth: [] },
          { headerAuth: [] },
          { queryAuth: [] },
          { basicAuth: [] },
          { jwt: [] },
        ],
      },
    },
    '/register': {
      post: {
        operationId: 'registerUser',
        requestBody: {
          $ref: '#/components/requestBodies/User',
        },
        responses: {
          201: {
            $ref: '#/components/responses/User',
          },
          default: {
            $ref: '#/components/responses/GeneralError',
          },
        },
      },
    },
  },
  tags: [
    {
      name: 'pet',
      description: 'Operations which apply to pets.',
    },
  ],
};

export default document;
