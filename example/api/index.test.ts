import { cloneDeep } from 'lodash';
import * as SwaggerParser from 'swagger-parser';

import api from '.';

it('should be a valid Swagger spec', async () => {
  await expect(SwaggerParser.validate(cloneDeep(api))).resolves.toBeDefined();
});
