import { verify } from 'jsonwebtoken';
import * as Koa from 'koa';
import * as logger from 'koa-logger';
import * as serve from 'koa-static';
import * as koasBodyParser from 'koas-body-parser';
import * as koas from 'koas-core';
import * as koasOperations from 'koas-operations';
import * as koasParameters from 'koas-parameters';
import * as koasSecurity from 'koas-security';
import * as koasSerializer from 'koas-serializer';
import * as koasSpecHandler from 'koas-spec-handler';
import * as koasStatusCode from 'koas-status-code';
import * as koasSwaggerUI from 'koas-swagger-ui';
import { join } from 'path';

import api from './api';
import createModels from './models';
import { User } from './models/User';
import * as operations from './operations';
import { Argv, JWTPayload } from './types';

export async function createServer({ database, secret }: Partial<Argv>): Promise<Koa> {
  const app = new Koa();
  app.keys = [secret];
  const models = await createModels(database);
  app.context.models = models;
  app.context.secret = secret;
  if (process.env.NODE_ENV !== 'test') {
    app.use(logger());
  }

  async function apiKeyAuth(apiKey: string): Promise<User> {
    return models.User.findOne({ where: { apiKey } });
  }

  app.use(serve(join(__dirname, 'static')));

  app.use(
    await koas(api, [
      koasSpecHandler(),
      koasSwaggerUI(),
      koasSecurity({
        cookieAuth: apiKeyAuth,
        headerAuth: apiKeyAuth,
        queryAuth: apiKeyAuth,
        basicAuth: (username, password) => models.User.findOne({ where: { username, password } }),
        jwt(accessToken: string) {
          let payload: JWTPayload;
          try {
            payload = verify(accessToken, secret) as JWTPayload;
          } catch (err) {
            return null;
          }
          return models.User.findOne({ where: { username: payload.sub } });
        },
        oauth2: () => null,
      }),
      koasStatusCode(),
      koasSerializer(),
      koasParameters(),
      koasBodyParser(),
      koasOperations({ operations }),
    ]),
  );
  return app;
}

export async function start(argv: Argv): Promise<void> {
  const { port } = argv;
  const app = await createServer(argv);
  app.listen(port, () => {
    // eslint-disable-next-line no-console
    console.log(`Listening on http://localhost:${port}`);
  });
}
